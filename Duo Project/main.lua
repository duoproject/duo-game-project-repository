local Gamestate = require "gamestate"
local anim8 = require "anim8"

local menu = Gamestate.new()
local credits = Gamestate.new()
local level1 = Gamestate.new()
local levelMenu = Gamestate.new()
local pause = Gamestate.new()

platform = {}
player = {}
pickUpGood = {}
pickUpBad = {}

function love.load()
  -- Load game states
 Gamestate.registerEvents()
 Gamestate.switch(menu)
 --Loads animation
 image1 = love.graphics.newImage("Sprites/Stickman-run.PNG")
 local g32 = anim8.newGrid(32, 33, 192, 33, 0, 0, 0)
 
 run = { anim8.newAnimation(g32('1-6', 1), 0.1) }
 run1 = { anim8.newAnimation(g32('1-6', 1), 0.1):flipH() }
 --Load sound files
 pickUpSound = love.audio.newSource("Music/PickUp.mp3")
 
 exitDoor = love.graphics.newImage('Sprites/portal.png')
 pickUpGood.img = love.graphics.newImage('Sprites/apple.png')
 pickUpBad.img = love.graphics.newImage('Sprites/burger.png')
 player.img = love.graphics.newImage('Sprites/Stickman.png')
 platformImage = love.graphics.newImage('Sprites/Platform.PNG')
end

function love.draw()
end

function love.update(dt)
  Gamestate.update(dt)
  love.keypressed(key)
end

function menu:init()
  menuMusic = love.audio.newSource("Music/MainMusic.ogg")
  
  title = love.graphics.newImage("MenuButtons/hungry-stickman.PNG")
  titleX = 50
  titleY = 80
  
  playButton = love.graphics.newImage('MenuButtons/play-game.PNG')
  playButtonX = 110
  playButtonY = 200
  
  quitButton = love.graphics.newImage('MenuButtons/quit-game.PNG')
  quitButtonX = 110
  quitButtonY = 500
  
  levelButton = love.graphics.newImage('MenuButtons/level-select.PNG')
  levelButtonX = 110
  levelButtonY = 300
  
  creditsButton = love.graphics.newImage('MenuButtons/credit-button.PNG')
  creditsButtonX = 110
  creditsButtonY = 400
end

function menu:draw()
    menuMusic:play()
    love.graphics.setBackgroundColor(200, 200, 250)
    love.graphics.draw(title, titleX, titleY)
    love.graphics.draw(playButton, playButtonX, playButtonY)
    love.graphics.draw(quitButton, quitButtonX, quitButtonY)
    love.graphics.draw(levelButton, levelButtonX, levelButtonY)
    love.graphics.draw(creditsButton, creditsButtonX, creditsButtonY)
end

function menu:update(dt)
  love.mousepressed(x, y, button, istouch)
end

function credits:init()
  
end

function credits:draw()
  love.graphics.setColor(0,0,0)
  love.graphics.print("Programmer - Kyle Twist", 10, 50)
  love.graphics.print("Stickman art - Gigobyte98", 10, 100)
  love.graphics.print("Main menu music - Brandon Morris", 10, 150)
  love.graphics.print("Game music - Kevin Macleod", 10, 200)
  love.graphics.print("Apple sprite - Vandrake", 10, 250)
  love.graphics.print("Burger sprite - Gobusto", 10, 300)
  love.graphics.print("Background - TokyoGeisha", 10, 350)
  love.graphics.print("Exit Portal - Selcan", 10, 400)
  love.graphics.print("Press M to go back to Main Menu", 10, 450)
  
end

function credits:update(dt)
  
end

function levelMenu:init()
  tutorialLevel = love.graphics.newImage("MenuButtons/Tutorial-Level.PNG")
  tutorialLevelX = 25
  tutorialLevelY = 25
end

function levelMenu:draw()
  love.graphics.draw(tutorialLevel, tutorialLevelX, tutorialLevelY)
end

function levelMenu:update(dt)
  love.mousepressed(x, y, button, istouch)
end

function level1:init()
  background = love.graphics.newImage('Sprites/background-2.PNG')
  gameMusic = love.audio.newSource("Music/GameMusic.mp3")
  
  platform.width = love.graphics.getWidth()
  platform.height = love.graphics.getHeight()
  platform.x = 0
  platform.y = 629
  
  floatPlatformX1 = 0 
  floatPlatformY1 = 550
  
  floatPlatformX2 = 0
  floatPlatformY2 = 150
  

  floatPlatformX3 = 0
  floatPlatformY3 = 150
  
  player.x = 10
  player.y = 615
  player.speed = 50
  player.ground = player.y
  player.y_velocity = 0
  player.jump_height = -300
  player.gravity = -500
  
  pickUpBad.width = 25
  pickUpBad.height = 25
  pickUpBad.x = 100
  pickUpBad.y = 600
  
  pickUpGood.width = 25
  pickUpGood.height = 25
  pickUpGood.x = 250
  pickUpGood.y = 600
  
  exitDoorWidth = 32
  exitDoorHeight = 32
  exitDoorX = 300
  exitDoorY = 600
  
  gameMusic:play()

end

function level1:draw()
  
  love.graphics.draw(background, 0, 0)
  love.graphics.rectangle('fill',platform.x, platform.y, platform.width, platform.height)
  love.graphics.draw(pickUpGood.img, pickUpGood.x, pickUpGood.y)
  love.graphics.draw(pickUpBad.img, pickUpBad.x, pickUpBad.y)
  love.graphics.draw(exitDoor, exitDoorX, exitDoorY)
  love.graphics.draw(platformImage, floatPlatformX1, floatPlatformY1)
  --love.graphics.setColor(0,0,0)
  love.graphics.print("A to move left.", 10, 50)
  love.graphics.print("D to move right.", 10, 100)
  love.graphics.print("Space to jump.", 10, 150)
  love.graphics.print("Eating a burger reduces speed and jump height.", 10, 200)
  love.graphics.print("Eating an apple increases speed and jump height.", 10, 250)
  love.graphics.print("Reach the portal to go to the next level.", 10, 300)
  love.graphics.print("Hold A and D together to Dance", 10, 350)
  if love.keyboard.isDown('d') == false then
    if love.keyboard.isDown('a') == false then
      love.graphics.draw(player.img, player.x, player.y , 0, 1, 1, 0, 32)
    end
  end

  if love.keyboard.isDown('d') then
    for i=1,#run do
      run[i]:draw(image1, player.x, player.y - 19)
    end
  end
  if love.keyboard.isDown('a') then
    for i=1,#run1 do
      run1[i]:draw(image1, player.x, player.y - 19)
    end
  end
end

function level1:update(dt) 
  
  gameMusic:play()
  
  collisions()
  
  if love.keyboard.isDown('d') then
    if player.x < (love.graphics.getWidth() - player.img:getWidth()) then 
     player.x = player.x + (player.speed * dt)
      for i=1,#run do
        run[i]:update(dt)
      end
    end
  end
  if love.keyboard.isDown('a') then
    if player.x > 0 then
      player.x = player.x - (player.speed * dt)
      for i=1,#run1 do
        run1[i]:update(dt)
      end
    end
  end
  if player.y_velocity ~= 0 then
    player.y = player.y + player.y_velocity * dt
    player.y_velocity = player.y_velocity - player.gravity * dt
  end
  
  if player.y > player.ground then
    player.y_velocity = 0
    player.y = player.ground
  end
end

function pause:init()
  
end

function pause:draw()
  love.graphics.print("Game is paused", 10, 50)
  love.graphics.print("Press L to unpause", 10, 100)
  love.graphics.print("Press M to go back to Main Menu", 10, 150)
end

function pause:update(dt)
  
end


function love.keypressed(key)
  
    if Gamestate.current() == level1 and key == 'p' then
        Gamestate.push(pause)
        gameMusic:stop()
    end
    if Gamestate.current() == pause and key == 'l' then
        Gamestate.push(level1)
    end
    if Gamestate.current() == pause and key == 'm' then
        Gamestate.push(menu)
    end
    if Gamestate.current() == credits and key == 'm' then
      Gamestate.push(menu)
    end
    if key == 'space' then
      if player.y_velocity == 0 then
        player.y_velocity = player.jump_height
      end
    end
    if key == 'm' then
      menuMusic:stop()
      gameMusic:stop()
    end
    
end

function love.mousepressed(x, y, button, istouch)
  
  if Gamestate.current() == menu then
    if button == 1 then
      if x < (playButtonX + 145) and x > playButtonX then
        if y < (playButtonY + 40) and y > playButtonY then
        Gamestate.push(level1)
        menuMusic:stop()
        end
      end
      if x < (quitButtonX + 145) and x > playButtonX then
        if y < (quitButtonY + 40) and y > quitButtonY then
          love.event.quit()
        end
      end
      if x < (levelButtonX + 145) and x > levelButtonX then
        if y < (levelButtonY + 40) and y > levelButtonY then
        Gamestate.push(levelMenu)
        menuMusic:stop()
        end
      end
      if x < (creditsButtonX + 110) and x > creditsButtonX then
        if y < (creditsButtonY + 40) and y > creditsButtonY then
        Gamestate.push(credits)
        menuMusic:stop()
        end
      end
    end
  end
  
  if Gamestate.current() == levelMenu then
    if button == 1 then
      if x < (tutorialLevelX + 70) and x > tutorialLevelX then
        if y < (tutorialLevelY + 128) and y > tutorialLevelY then
        Gamestate.push(level1)
        menuMusic:stop()
        end
      end
    end
  end
  
end


function collisions()
  
  hitTest = CheckCollision(pickUpGood.x, pickUpGood.y, pickUpGood.width, pickUpGood.height, player.x, player.y, 20, 40)
  if(hitTest) then
    player.jump_height = -500
    player.speed = 75
    pickUpGood.y = -2000
    pickUpSound:play()
  end
  hitTest1 = CheckCollision(pickUpBad.x, pickUpBad.y, pickUpBad.width, pickUpBad.height, player.x, player.y, 20, 40)
  if (hitTest1) then
    player.jump_height = -100
    player.speed = 25
    pickUpBad.y = -2000
    pickUpSound:play()
  end
  
  hitTest2 = CheckCollision(floatPlatformX1, floatPlatformY1, 100, 20, player.x, player.y, 20, 40)
  if (hitTest2) then
    if player.x < (floatPlatformX1 + 100) and player.x > floatPlatformX1 then
      player.ground = floatPlatformY1 - 14
    end
  else if player.x > (floatPlatformX1 + 100) then
    player.ground = 619
  end
end

  hitTest3 = CheckCollision(exitDoorX, exitDoorY, 32, 32, player.x, player.y, 20, 40)
  if (hitTest3) then
    Gamestate.push(credits)
    gameMusic:stop()
  end
end

function CheckCollision(x1,y1,w1,h1, x2,y2,w2,h2)
  return x1 < x2+w2 and
         x2 < x1+w1 and
         y1 < y2+h2 and
         y2 < y1+h1
end
